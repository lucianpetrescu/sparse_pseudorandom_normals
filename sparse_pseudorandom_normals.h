///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module needs OpenGL and wic provides it
#include "dependencies/lap_wic/lap_wic.hpp"
#include "core/gpu_texture.h"
#include "core/gpu_shader.h"

namespace spn {

	class SparsePseudoRandomNormals {
	public:

		enum class Algorithm {Vicinity3, Vicinity5, Box5, Radial, PseudoRandomNormals};
		
		//ctor, dtor
		SparsePseudoRandomNormals();
		~SparsePseudoRandomNormals();
		
		//set input
		void setInput(GLuint texid);
		void setInput(GPUTexture2D* input);
		
		void computeNormals(Algorithm algo);

		GPUTexture2D* getOutput();
		GLuint getOutputGL();
		const std::string getAlgorithmName(Algorithm algo);

		//useful for reloading and debugging shaders
		void debugReloadShaders();
	private:
		struct {
			int width = 1 ;
			int height = 1;
			GLuint texid = 0;
		}input;
		GPUTextureSampler sampler_near;

		GPUShader shader_normals;
		GPUTexture2D gpu_tex;
	};
}
