# README #

LAP WIC is a ease-of-use micro window toolkit for OpenGL, which wraps a modified version of GLFW 3.2 (http://www.glfw.org/) 
and a modified version of the GLAD extension loader (https://github.com/Dav1dde/glad). Compared to the original libraries LAP WIC
makes using GLFW and GLAD functionality as simple as including a .h and a .cpp file into your project. Therefore, the user controls 
the compiler, the compiler settings, and the entire deployment process. Test examples are provided with the library. 
LAPWIC focuses on minimizing user effort by offering an object oriented structure with modern callback capabilities (member functions, 
lambdas, etc), minimal threading restrictions (create many windows from different threads and run them concurrently - but don't forget
to synchronize OpenGL access), the option of using events instead of callbacks, background multi-context multi-thread OpenGL extension
loading and management and a streamlined window creation process based on requested properties.
version 1.10

### SCREENSHOTS ###

Basic Usage

 ![](img/1basic_usage.png)

Window Properties (almost all members can be specified before window creation)

 ![](img/2window_properties.png)

Framebuffer Properties (all members can be specified before window creation)

 ![](img/3framebuffer_properties.png)


Context Properties (almost all members can be specified before window creation)

 ![](img/4context_properties.png)

Input Properties (all members can be specified before window creation)

 ![](img/5input_properties.png)

Input State (the state of the input - per window!)

 ![](img/6input_state.png)

Event Types, the types of events received by WIC windows

 ![](img/7event_types.png)

If callbacks are preferred to the event input method used in "Basic Usage" then several methods are available: free functions, functors, delegates, std::bindables or lambdas, basically anything storable in a std::function.

 ![](img/8types_of_callbacks.png)

A single window running a basic OpenGL program

 ![](img/9simple_opengl.png)

Multiple windows with different contexts sharing OpenGL resources (the same geometry stored in shared VBOs is rendered differently in each window)

 ![](img/10multiple_windows.png)

As working with windows is limited only to the main thread on some platforms, the WICSystem runs a hidden client-server architecture which enables multithreaded usage. This is valid only if the user decides to create a multithreaded WICSystem (by default it is singlethreaded). Windows created outside the main thread use blocking commands (highlighted in the documentation) while windows created on the main thread completely bypass the protocol. An abstract example of this protocol is shown in the next image. While this may sound expensive, the rate of the blocking commands (e.g. create, destroy, resize, change title, etc) is in practice several orders of magnitude lower than that of rendering commands or context commands. This type of architecture is useful for multithreaded multi-window multi-context applications (e.g. many debuggers, loggers or monitoring tools benefit from this design).

 ![](img/11multithreaded_usage.png)

### CONFIGURATION ###
Lap\_wic can be configured from the configure section in lap_wic.hpp (lines 4-13).

OpenGL tokens and core functions extension loading can be included by defining (default = defined):

	#define LAP_WIC_INCLUDE_GL_EXTENSIONS	(line 5 of lap_wic.hpp)

If another OpenGL extension loading library is to be used the previous token has to be undefined.

When building on *nix systems the default display server is X11. 
The display server can be forced to Wayland be defining this macro (default = undefined):

	#define LAP_WIC_FORCE_NIX_WAYLAND (line 7 of lap_wic.hpp)

The display server can be forced to Mir be defining this macro (default = undefined):

	#define LAP_WIC_FORCE_NIX_MIR (line 9 of lap_wic.hpp)

Vulkan functions can be included by defining (default = undefined):

	#define LAP_WIC_INCLUDE_VULKAN (line 11 of lap_wic.hpp)
	
The Vulkan loader can be linked statically into application by defining (default = undefined):
	
	#define LAP_WIC_VULKAN_STATIC (line 13 of lap_wic.hpp)



### DESIGN AND THREADING RESTRICTIONS ###
In singlethreaded mode lap\_wic works exactly like any other windowing toolkit, a WICSystem object has to be created for the entire
program (singleton), which initializes the necessary system queries. After this windows can be created, controlled and used for rendering.
In multithreaded mode lap_wic works as a client-server architecture with the program-wide WICSYstem object acting as server and 
WIndow objects created outside of the main thread acting as clients. This design is required because there are several platform limitations 
which are impossible to bypass in a clean multi-threaded manner. Not all window commands require this type of protocol, the ones working
in this manner are documented. Other designs are possible : single threaded only or taking control of the main thread, like (free)glut, but
leaves the maximum programmer flexbility. The consequences of this design are the following:

-	When the WICSystem object is destroyed, it WAITS for the destructors of all the registered windows, thus Window objects have to go out of 
scope during the lifetime of WICSystem, in a standard OOP manner.
-	commands will only be executed after the server has finished processing them. This processing is done by continuously calling the 
WicSystem.processProgramEvents() function from the main thread. DON'T BLOCK THE MAIN THREAD WHILE USING WIC.
-	WICSystem is a singleton, it will assert if the user tries to create more than one instance.
-	WICSystem.processProgramEvents(), WICSystem.WICSystem(), WICSystem.~WICSystem() can only be called form the main thread (assert otherwise)


###OPENGL###
IMPORTANT: The lap\_wic OpenGL function extension loader only loads functions and tokens which are included in the compatibility profie or are 
ubiquitous. Lap\_wic contains functions which enable manual function extension loading.

The lap\_wic library is by default ran in a thread-safe manner but OpenGL access is NOT thread safe and NEEDS manual synchronization.
Therefore all context operations (window.swapBuffers(), window.setSwapInterval(), window.bindContext() and window.unbindContext())
and OpenGL commands (glCreateShader, glBufferData, etc) require synchronized access, if rendering is performed concurrently from multiple
threads. The context functions are properly documented.
For more information consult the specification, section 2.3.: https://www.opengl.org/registry/doc/glspec45.core.pdf

Quick list of important concepts:

- in OpenGL there can be only one context per thread
- an OpenGL command issued on a contextless thread results in undefined behavior
- context switches are costly as they save/restore ALL state and cause a GPU flush. On some platforms even the function pointers have to be reloaded!.
- your application must ensure that two tasks that access the same context are not allowed to execute concurrently. This is valid on both CPU
where context are binded/unbinded per thread (with explicit synchronization, e.g.: locks, lockless) and on the GPU (where objects shared between
multiple contexts have to be synchronized with sync/fances).
- if issueing OpenGL commands concurrently from multiple threads, each with their own context, it is very probable that the GPU driver
will perform a hidden synchronization, as internally OpenGL drivers are generally single threaded. Thus it is usually most efficient to have a 
single OpenGL thread. Multithreaded rendering can be performed by using non immediate contexts which hold queues of OpenGL commands, and 
collecting the commands with a single OpenGL rendering thread (this technique emilates DirectX11)


###NOVEL ASPECTS###

While there are other adequate windowing toolkits availble for free, this toolkit offers several new features:\n

-	absolutely no deployment issues, besides linking with the system libraries (and vulkan if necessary/possible). LAPWIC is plug play,
just insert the lap\_wic.hpp, lap\_wic\_dep.hpp and lap\_wic.cpp into your build system. No libs, no compilers, no dlls, no libs, no exports.
-	minimal threading restrictions. Most windowing toolkit libraries limit the window creation and event processing to the main thread,
usually due to windowing system limitations. LAPWIC asks only for a single function to be constantly called from the main thread, 
(WICSystem.processProgramEvents()), besides this function and WICSystem creation and destruction any function can be used from everywhere.
The threading restricted functions are documented, and perform internal system initializations and application wide input processing.
For a more clear program structure see the examples shown in this file or test.cpp.
LAPWIC in multithreading is dependent on the WICSystem.processProgramEvents() function, thus the user has to guarantee that this function
gets executed. E.g.: the user creates a window from a secondary thread, but has to guarantee that the main thread is running when the
secondary thread is creating the window.
-	integrated multi-context multi-thread extension loading. LAPWIC can be used from many threads with different OpenGL contexts, 
in comparison to the majority of other extension loader libraries. The extension are handled in a cached manner, thus a change
of context only implies a change of function pointers and a pipeline flush. Thus a context switch is expensive (the pipeline flush) but 
not prohibitively expensive.
-	a flexibile object oriented input and windows event system. The events can be handled through modern callbacks, which accept lambdas, 
delegates, functors, basically anything storable in a std::function, or through events, which come with window information, input state
information and a timestamp.
-	LAPWIC makes it extremely easy to decouple a rendering thread with window control from an input processing thread.
-	each window can run its own main function, completely decoupled from other windows.
Besides the novel features presented here LAPWIC provides almost all the features from both GLFW and GLAD, along with minor bugfixes\n

###PROPERTIES###
-	c++11
-	works on Windows, *nix and MACOS
-	object oriented
-	window, input, monitors, context creation and vulkan access and handling through GLFW (http://www.glfw.org/)
-	OpenGL extensions loading through GLAD (https://github.com/Dav1dde/glad)
- OpenGL extensions can be exported as macros or as functions (by default global namespace functions). This can be changed by changing gl_as_functions from True to False in dep_gen/generator.py and then running the script)
	- Macros are a C-style solution, they can be undefined, lead to more coupled code, can't be shadowed (e.g. preprocessor will transform struct.glCullFace into struct.loader_glCullFace). They are used by:
		- glad (https://github.com/Dav1dde/glad)
		- glsdk (http://glsdk.sourceforge.net/docs/html/index.html) if using c mode
	- Functions are a CPP-style solution, they support intelligent code completion, can be used as "extern" from other files without including the interface, lead to increased code decoupling, can be shadowed (but don't compile with -Wshadow). They are used by:
		- glew 2.0 (http://glew.sourceforge.net/)
		- glbinding (https://github.com/cginternals/glbinding) , under gl namespace.
		- gl3w (https://github.com/skaslev/gl3w)
		- glsdk (http://glsdk.sourceforge.net/docs/html/index.html) if using cpp mode, under the gl namespace
-	minimal size (include in executable only what you use)
-	minimal deployment, just plug and play.
-	minimal threading restrictions, just the following functions need to be called from the main thread
	-	WICSystem.processProgramEvents()
	-	WICSystem.WICSystem()
	-	WICSystem.~WICSystem() 
-	RAII window creation from any thread.
-	window events and input processing with either callbacks or events
-	modern callbacks
	-	delegates / member functions
	-	lambdas
	-	functions
	-	functors 
	-	anything captureable with std::function
-	cached multi-context multi-thread OpenGL extension loading and management.
- input remapper (keys, mouse buttons, controller buttons)
-	each window has it's own main and can be controlled independently
-	streamlined window creation process : windows are created through requested property structs:
	-	WindowProperties : requested properties of the system window such as decorated, focused, visible, size, position, etc.
	-	FramebufferProperties : channel bit depths, double buffering, accumulation buffers for legacy OpenGL, etc
	-	ContextProperties : type of context, debug options, requested version, etc
	-	InputProperties : cursor enabled? sticky keys/cursor? etc
-	the event system provides direct access to the window input state
-	all keys and mouse buttons offer access to the last pressed timestamp
-	lapwic can be used as a windowing toolkit without OpenGL
-	examples are provided in test.cpp \n

###COMPILING AND LINKING###

- 	Windows:
	- 	no special setup for visual studio projects
	- 	-pthread -lgdi32 otherwise
	- 	an example visual studio project+solution is provide in the vstudio folder
- 	Linux:
	- 	-ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- 	the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- 	an example makefile is provided in the root folder
	-	 to install the project dependencies get:
		-	 sudo apt-get install xorg-dev
		-	 sudo apt-get install libgl-dev
		- 	also works in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html.
- 	OSX
	- 	-framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- 	an example makefile is provided in the root folder\n

###LIMITATIONS AND OBSERVATIONS###

- 	the main thread MUST always run a main program loop, like: 
	while (WICSystem.isAnyWindowOpened()) {
		WICSystem.processProgramEvents();
	}
- 	windows from non-main threads use a blocking type of communication with the main thread, which is hidden from the library user,
thus the main thread should NEVER block inside the loop.
- 	the almost lack of threading limitations has the positive aspect of potentially specializing the main thread for input handling
- 	as LAPWIC depends on GLFW, please read http://www.glfw.org/faq.html and https://github.com/glfw/glfw/issues if you are encountering unexpected difficulties.
- 	do not include Windows.h or other platform specific headers unless you plan on using those APIs directly, if you do need to include such headers, do it before including the GLFW.\n

###TODO LIST###

more testing on osx.


### EXAMPLE - SIMPLE###

		#include "lap_wic.hpp"
		...
		using namespace lap::wic;
		WICSystem wicsystem(&std::cout);
		Window window = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
		...
		bool running = true;
		while (window.isOpened() && running) {
			//DO WORK HERE
			window.swapBuffers();
			while (const Event* e = window.processEvent()) {
				if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) running = false;
				if (e->type == Event::Type::MouseMove) std::cout << "Mouse move at " << e->MouseMove.position_x << " " << e->MouseMove.position_y << std::endl;
			}
			WICManager::processProgramEvents();
		}
		
### EXAMPLE - CALLBACKS###

		#include "lap_wic.hpp"
		...
		using namespace lap::wic;
		WICSystem wicsystem(&std::cout);
		Window window = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
		window.setCallbackMouseScroll([](const Window& caller, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState&, uint64_t timestamp) {
			std::cout << "[Lambda Callback] Window " << caller.getWindowProperties().title << " Mouse scroll " << scrolly << std::endl;
		});
		...
		while (window.isOpened()) {
			//DO WORK HERE
			window.swapBuffers();
			window.processEvents();
			WICManager::processProgramEvents();
		}

### EXAMPLE - MULTIPLE WINDOWS ###

		#include "lap_wic.hpp"
		...
		using namespace lap::wic;
		WICSystem wicsystem(&std::cout);
		WindowProperties wp1; wp1.title = "first window";
		Window window1 = Window(wp1, FramebufferProperties(), ContextProperties(), InputProperties());
		WindowProperties wp2; wp2.title = "seconds window";
		Window window2 = Window(wp2, FramebufferProperties(), ContextProperties(), InputProperties());
		...
		while (WICManager::isAnyWindowOpened()) {
			if(window1.isOpened()){
				//DO WORK HERE
				window1.swapBuffers();
				while (const Event* e = window1.processEvent()) {
					//process events here
				}
			}
			if(window2.isOpened()){
				//DO WORK HERE
				window2.swapBuffers();
				while (const Event* e = window2.processEvent()) {
					//process events here
				}
			}
			WICManager::processProgramEvents();
		}

### EXAMPLE - SHARED OPENGL CONTEXT ###

		#include "lap_wic.hpp"
		...
		using namespace lap::wic;
		WICSystem wicsystem(&std::cout);
		WindowProperties wp1; wp1.title = "first window";
		Window window1 = Window(wp1, FramebufferProperties(), ContextProperties(), InputProperties());
		WindowProperties wp2; wp2.title = "seconds window";
		Window window2 = Window(wp2, FramebufferProperties(), ContextProperties(), InputProperties(), nullptr, &window1);
		...
		window1.bindContext();
		//DO OPENGL WORK HERE ON CONTEXT 1
		...
		window2.bindContext(); //shared lists with window 1
		bool running = true;
		while (window2.isOpened() && running) {
			//DO OPENGL WORK HERE ON CONTEXT 2 USING WORK FROM CONTEXT 1 (shared)
			window2.swapBuffers();
			while (const Event* e = window2.processEvent()) {
				if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) running = false;
			}
			WICManager::processProgramEvents();
		}

### EXAMPLE - TOGGLE FULLSCREEN ###

		#include "lap_wic.hpp"
		...
		using namespace lap::wic;
		WICSystem wicsystem(&std::cout);
		Window window = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
		...
		while (window.isOpened()) {
			//DO WORK HERE
			window.swapBuffers();
			while (const Event* e = window.processEvent()) {
				if (e->type == Event::Type::KeyPress) {
					if (e->KeyPress.key == Key::F1) window.setFullscreen();
					if (e->KeyPress.key == Key::F2) window.setWindowed(400, 400, 100, 100);
				}
			}
			WICManager::processProgramEvents();
		}
		
### EXAMPLE - MANUAL EXTENSION LOADING###
		
		///NOTE: lapwic only loads the OpenGL core / compatibility functions and the ubiquitous extensions
		#include "lap_wic.hpp"
		...
		using namespace lap::wic;
		WICSystem wicsystem(&std::cout);
		Window window1 = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
		...
		supported = window.isOpenGLExtensionSupported("GL_ARB_sparse_buffer");
		std::cout << " GL_arb_sparse_buffer support = " << std::boolalpha << supported << std::endl;
		if (supported) {
			typedef void (APIENTRYP PFNGLBUFFERPAGECOMMITMENTARBPROC)(GLenum target, GLintptr offset, GLsizeiptr size, GLboolean commit);
			PFNGLBUFFERPAGECOMMITMENTARBPROC my_glBufferPageCommitmentARB = (PFNGLBUFFERPAGECOMMITMENTARBPROC)window.loadOpenGLExtensionFunction("glBufferPageCommitmentARB");
			// do work here but don't forget this function pointer is unmanaged and might be (usually isn't) valid only for this context
		}
		...
		@endcode

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 
Note: doxygen has problems with arguments of std::function type function arguments (used for callbacks) and will issue a large number of warnings.


### COMPATIBILITY ###

the version based on the 3.2.0 generator (generator320.py in dep\_gen) was tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.

the version based on the the 3.2.1 generator (generator321.py in dep\_gen) was tested with visual studio 2015, MinGW x64 4.0. 

### TODO ###
3.2.1 testing with macos + x11 + wayland + mir.