## Sparse Pseudorandom Normals on the GPU##

NOTE1: this algorithm is in the process of being published.  
NOTE2: the normals are NOT filtered after calculation in order to better compare the base algorithms.

![img](img/normals-pseudorand.png)

VIDEO: here the normals computed with the sparse pseudorandom normals algorithm are stochastically filtered, and depth stabilization is used to increase the quality of the input, resulting in high quality normals with minimal structured artifacts. 
[![](http://drive.google.com/uc?export=view&id=0B3yTES0pEsWXa1l6Z1UyMzZldWM)](https://drive.google.com/open?id=0B3yTES0pEsWXb0FzNVE2WUluZlE "link to video")

This project implements a fast algorithm which computes normals on depth from RGBD cameras. Because the output from RGBD cameras is imperfect, computing high quality normals is difficult without heavy processing. There are several strategies to compute high quality normals on a badly sampled dataset.

High quality state-of-the-art methods usually use some form of image labeling or at least some clustering to determine the general structure of the sampled input and then analytically determine the normals over this structure. Temporal filtering and even tridimensional reconstruction can be used to produce highly accurate results but these strategies are very costly, as the extra samples can only be obtained after correctly estimating the camera pose estimation from the previous frame (with [SLAM](https://en.wikipedia.org/wiki/Simultaneous_localization_and_mapping), [visual odometry](https://en.wikipedia.org/wiki/Visual_odometry), etc). 

Fast methods use averaging (direct or through image pyramids/mipmaps), but these are prone to propagating structured artifacts which might or might not produce results of acceptable quality. The problem of accurate normal computation is even more difficult if the result has to be obtained with a very low computational cost, as fast methods using a small number of samples become extremely prone to structured noise. 

The presented algorithm is a fast averaging-based method, which uses a sparse sampling strategy in order to minimize the number of samples needed to quickly cover the vicinity of the processed pixel. The samples are used to compute multi-resolution normals, which are then treated as in input set. The statistical outliers are removed and then the result is computed as an average over the remaining normals.  

Several other fast algorithms are implemented in this project.

NOTE: OpenGL 4.5 compatible hardware is required to run this project.       


#### ALGORITHM ####

To accurately compute normals on a noisy depth input, averaging methods use multiple  multiresolution normals. This is done through high frequency sampling, the most efficient being summed area tables (SATs, also known as mipmaps). Unfortunately a large number of samples still implies a large computational cost, as real-time RGBD camera feeds leave no time for such preprocessing  (in contrast with the offline tools used to compute mipmaps for static textures).

The sparse pseudorandom normal algorithm uses sparse sampling, mapping a large vicinty of the pixel to the [0-1]x[0-1] domain and then generating sampling locations in this parameterized space with quasi random [low-discrepancy sequences](https://en.wikipedia.org/wiki/Low-discrepancy_sequence#Hammersley_set) like [the Sobol sequence](https://en.wikipedia.org/wiki/Sobol_sequence), the [Poisson disk](https://en.wikipedia.org/wiki/Low-discrepancy_sequence#Poisson_disk_sampling), the [Halton sequence](https://en.wikipedia.org/wiki/Halton_sequence) or the [Van der Corput sequence](https://en.wikipedia.org/wiki/Van_der_Corput_sequence). These sequences have the property of uniformly but pseudo-randomly covering the parameterized vicinity space. These samples are then used to generate a set of normals, using both close and far away samples in the vicinity. Then the normals have the outliers stochastically rejected and the final normal is computed by averaging.

Compared to SAT or other averaging methods, this method uses significantly less samples (e.g. 16 instead of 16x16) and also rejects samples which have a high probability of being erroneous. 

The results presented here can be greatly improved with stochastic filtering over the final results and by filtering and stabilizing the depth signal, but those are outside of the scope of this example.

#### CONTROLS ####

The application does not have a GUI in order to minimize code. The application is controlled through the keyboard. Here is screenshot of the console after pressing the H key :

![help](img/help.png)

### ALL SCREENSHOTS ###

input position    
![help](img/position.png)    
input depth  
![help](img/depth.png)  
normals in 3x3 vicinity  
![help](img/normal-vicinity3.png)    
normals in 5x5 vicinity  
![help](img/normal-vicinity5.png)  
normals using box  
![help](img/normal-box5.png)    
normals using a radial algorithm  
![help](img/normal-radial.png)  
normals with pseudo-random sampling    
![help](img/normals-pseudorand.png)  

#### BUILDING and DEPENDENCIES ####

This project uses [lap_wic](https://bitbucket.org/lucianpetrescu/public) for all OpenGL context, input handling and windowing needs and [glm](http://glm.g-truc.net/0.9.8/index.html) for mathematics. 
The project should compile with any CPP11 compiler. A 2015 visual studio project is prodived in the /vstudio folder. makefiles for visual studio and make+GCC can be found in the root folder. This project requires an OpenGL 4.5 graphics card with up-to-date drivers 

Tested under Windows (vstudio and gcc with mingw).

#### LICENSE ####

The MIT License (MIT)
 
Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.