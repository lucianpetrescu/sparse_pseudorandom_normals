///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "renderer.h"
#include "file_manager.h"
#include "core/gpu_timer.h"
#include "sparse_pseudorandom_normals.h"
#include <iomanip>
#include <algorithm>

namespace spn {
	class App {
		Renderer renderer;
		FileManager file_manager;
		struct Timing{
			float cpu =0;
			float gpu =0;
		};
		struct {
			GPUTimer timer{ 2 };
			float frame_cpu = 0, frame_gpu = 0;
			float num_frames = 0;
			void reset() {
				num_frames = 0;
				frame_cpu = frame_gpu = 0;
			}
		}timings;

		//edge detector
		SparsePseudoRandomNormals normal_computer; 
		struct {
			std::vector<SparsePseudoRandomNormals::Algorithm> normal_algorithms_list = {
				SparsePseudoRandomNormals::Algorithm::PseudoRandomNormals,
				SparsePseudoRandomNormals::Algorithm::Radial,
				SparsePseudoRandomNormals::Algorithm::Box5,
				SparsePseudoRandomNormals::Algorithm::Vicinity5,
				SparsePseudoRandomNormals::Algorithm::Vicinity3
			};
			int normal_algorithm_index = 0;
			Renderer::RenderTarget render_target = Renderer::RenderTarget::Normals;
		}options;
		
	public:

		///----------------------------------------------------------------------------------------------------------------------
		App(unsigned int width, unsigned int height) {
			timings.timer.reset();

			//asset paths
			file_manager.setAssetPath("../input/");
			file_manager.setFile("1.pos");
			renderer.setOutputPath("../output/");

			//renderer
			renderer.resize(width, height);
		}
		~App() {
		}
		void render() {
			//set algorithm input
			normal_computer.setInput(file_manager.getFileOutput().gpudata);

			//run and time edge detection
			timings.timer.insertEntry(0);
			auto algo = options.normal_algorithms_list[options.normal_algorithm_index];
			normal_computer.computeNormals(algo);
			timings.timer.insertEntry(1);

			//average timings
			timings.timer.synchronizeWithGPU();
			timings.frame_cpu = (timings.frame_cpu * timings.num_frames + timings.timer.getTimeBetweenEntriesInMillisecondsCPU(0, 1)) / (timings.num_frames + 1.0f);
			timings.frame_gpu = (timings.frame_gpu * timings.num_frames + timings.timer.getTimeBetweenEntriesInMillisecondsGPU(0, 1)) / (timings.num_frames + 1.0f);
			if (timings.num_frames < 1000000.f) timings.num_frames++;

			//render full(virtual)screen on window
			renderer.setInput(file_manager.getFileOutput().gpudata, normal_computer.getOutput());
			renderer.render(options.render_target);
		}
		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			renderer.resize(width, height);
		}

		///----------------------------------------------------------------------------------------------------------------------
		void printResults() {
			//present results
			std::cout << " -------------------- RESULTS --------------------" << std::endl;
			std::cout << " Timed over "<<(int)timings.num_frames<<" frames on input from "<<std::endl<<" <"<<file_manager.getFileOutput().filename<<"> : " << std::endl;
			std::cout << std::fixed<< std::setprecision(5) << " Total execution time CPU = " << timings.frame_cpu << " ms   GPU = " << timings.frame_gpu << " ms" << std::endl;
		}

		///----------------------------------------------------------------------------------------------------------------------
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			static bool window_resizeable = true;
			switch (key) {

			//debug
			case lap::wic::Key::SPACE:
				normal_computer.debugReloadShaders();
			break;

			//close app
			case lap::wic::Key::ESCAPE: wnd.close();				break;	// the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed

			//cycle file manager input and run
			case lap::wic::Key::LEFT:
				file_manager.cycleFilePrev();
				timings.reset();
				if (window_resizeable) wnd.setSize(std::max(file_manager.getWidth(),(unsigned int)100), std::max(file_manager.getHeight(), (unsigned int)100));
				break;
			case lap::wic::Key::RIGHT:
				file_manager.cycleFileNext();
				timings.reset();
				if(window_resizeable) wnd.setSize(std::max(file_manager.getWidth(), (unsigned int)100), std::max(file_manager.getHeight(), (unsigned int)100));
				break;
			case lap::wic::Key::P: printResults();	break;

				//various
			case lap::wic::Key::W: 
				window_resizeable = !window_resizeable;	
				std::cout << " window is now " << ((!window_resizeable) ? "not" : "") << " resizable." << std::endl;
				break;
			case lap::wic::Key::S : {
				static unsigned int index = 0;
				renderer.screenshot("out" + std::to_string(index) + ".png");	//saved in render output path + filename -> output/outINDEX.png
				index++;
			}	break;
			case lap::wic::Key::H: {
				std::cout << std::endl << "----------------------------------------------------" << std::endl;
				std::cout << "----------------------- HELP -----------------------" << std::endl;
				std::cout << "GENERAL: " << std::endl;
				std::cout << "  x,z        - cycle through normal computation algorithm" << std::endl;
				std::cout << "  up down    - cycle through outputs (depth, positions, normals)" << std::endl;
				std::cout << "  left right - cycle through image files in the input folder (/input/)" << std::endl;
				std::cout << "  p          - print time measurements (per stage, per algorithm)" << std::endl;
				std::cout << "  esc        - close application" << std::endl;
				std::cout << "VARIOUS: " << std::endl;
				std::cout << "  space      - reload all GLSL shaders (useful for debug)" << std::endl;
				std::cout << "  w          - enables / disables window resize on file change" << std::endl;
				std::cout << "  s          - save a screenshot in the output folder (/output/)" << std::endl;

			}	break;


			//toggle operator
			case lap::wic::Key::X: {
				//toggle operator
				options.normal_algorithm_index = (options.normal_algorithm_index + 1) % options.normal_algorithms_list.size();
				auto algo = options.normal_algorithms_list[options.normal_algorithm_index];
				std::cout << " Using <"<<normal_computer.getAlgorithmName(algo)<<"> for normal computation" << std::endl;
				timings.reset();
			}	break;
			case lap::wic::Key::Z: {
				//toggle operator
				options.normal_algorithm_index = (options.normal_algorithm_index - 1 + (int)options.normal_algorithms_list.size()) % options.normal_algorithms_list.size();
				auto algo = options.normal_algorithms_list[options.normal_algorithm_index];
				std::cout << " Using <" << normal_computer.getAlgorithmName(algo) << ">  for normal computation" << std::endl;
				timings.reset();
			}	break;

			//cycle detector
			case lap::wic::Key::DOWN:
				timings.reset();
				switch (options.render_target) {
				case Renderer::RenderTarget::Depth:
					options.render_target = Renderer::RenderTarget::Normals;
					std::cout << " Showing Normals (0-1)" << std::endl;
					break;
				case Renderer::RenderTarget::Positions:
					options.render_target = Renderer::RenderTarget::Depth;
					std::cout << " Showing Depth (0-1)" << std::endl;
					break;
				case Renderer::RenderTarget::Normals:
					options.render_target = Renderer::RenderTarget::Positions;
					std::cout << " Showing Positions (0-1)" << std::endl;
					break;
				}
				break;
			case lap::wic::Key::UP:
				timings.reset();
				switch (options.render_target) {
				case Renderer::RenderTarget::Depth:
					options.render_target = Renderer::RenderTarget::Positions;
					std::cout << " Showing Positions (0-1)" << std::endl;
					break;
				case Renderer::RenderTarget::Positions:
					options.render_target = Renderer::RenderTarget::Normals;
					std::cout << " Showing Normals (0-1)" << std::endl;
					break;
				case Renderer::RenderTarget::Normals:
					options.render_target = Renderer::RenderTarget::Depth;
					std::cout << " Showing Depth (0-1)" << std::endl;
					break;
				}
				break;

			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	};
}



//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
	
	//ignore some type of warnings (otherwise the glsl compiler might spam the console with notifications)
	if (type == GL_DEBUG_TYPE_PERFORMANCE || type == GL_DEBUG_TYPE_PORTABILITY || severity == GL_DEBUG_SEVERITY_NOTIFICATION || type == GL_DEBUG_TYPE_OTHER) return;

	std::cout << "-------------------------------------------------------------------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cin.get();
	std::cout << std::endl;
}


int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "Sparse Pseudo-Random Normals";						
	wp.width = 640;			wp.height = 480;
	wp.position_x = 1000;	wp.position_y = 200;
	fp.samples_per_pixel = 1;	//request a single sample per pixel
	cp.swap_interval = -1;
	cp.debug_context = true;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);
	std::cout << std::endl << "----------------------------------------------" << std::endl;
	std::cout << std::endl << "----------------------------------------------" << std::endl;
	std::cout << std::endl << "----------------------------------------------" << std::endl;

	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	
	//edge App object + delegates
	spn::App app(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&spn::App::resize, &app, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&spn::App::keyPress, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&spn::App::keyRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&spn::App::keyRepeat, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&spn::App::mousePress, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&spn::App::mouseRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&spn::App::mouseDrag, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&spn::App::mouseMove, &app, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&spn::App::mouseScroll, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		app.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};

}