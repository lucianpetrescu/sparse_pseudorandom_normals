#version 440
layout(location = 0) out vec4 out_color;

layout(binding = 0) uniform sampler2D sampler_positions;
layout(binding = 1) uniform sampler2D sampler_normals;
uniform int render_target;
in vec2 texcoord;

void main(){
	switch(render_target){
		case 0:
			//depth
			out_color = vec4(texture(sampler_positions, texcoord).zzz / vec3(8.0,8.0,8.0),1);
			break;
		case 1:
			//positions
			out_color = vec4(texture(sampler_positions, texcoord).xyz / vec3(3.0, 3.0, 3.0),1);
			break;
		case 2:
			//normals
			vec3 n = texture(sampler_normals, texcoord).xyy;
			n.z = sqrt(1 - n.x*n.x - n.y * n.y);
			out_color = vec4( (n + vec3(1,1,1)) / 2.0 , 1);
			break;
		default:
			break;
	}
}