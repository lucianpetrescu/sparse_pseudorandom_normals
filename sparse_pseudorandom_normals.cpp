///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "sparse_pseudorandom_normals.h"

namespace spn {
	SparsePseudoRandomNormals::SparsePseudoRandomNormals() {
		//samplers
		sampler_near.setClamp();
		sampler_near.setNearest();

		//gaussian blurring
		shader_normals.load("../shaders/normals.comp");
		gpu_tex.create(GL_RG8, 10, 10, false);
	}
	SparsePseudoRandomNormals::~SparsePseudoRandomNormals() {
	}

	void SparsePseudoRandomNormals::setInput(GLuint texid) {
		int w, h;
		glGetTextureLevelParameteriv(texid, 0, GL_TEXTURE_WIDTH, &w);
		glGetTextureLevelParameteriv(texid, 0, GL_TEXTURE_HEIGHT, &h);
		input.width = w;
		input.height = h;
		input.texid = texid;
	}
	void SparsePseudoRandomNormals::setInput(GPUTexture2D* in_input) {
		//input
		input.width = in_input->getWidth();
		input.height = in_input->getHeight();
		input.texid = in_input->getGLobject();

		//resize?
		if (input.width != gpu_tex.getWidth() || input.height != gpu_tex.getHeight()) {
			gpu_tex.create(GL_RG8, input.width, input.height, false);
		}
	}
	void SparsePseudoRandomNormals::computeNormals(Algorithm algo){
		unsigned int w = input.width;
		unsigned int h = input.height;

		//compute normals
		glBindTextureUnit(0, input.texid);
		sampler_near.bind(0);
		shader_normals.bind();
		switch (algo) {
		case Algorithm::Vicinity3:
			shader_normals.setSubroutine("algorithmVicinity3", GL_COMPUTE_SHADER);
			break;
		case Algorithm::Vicinity5:
			shader_normals.setSubroutine("algorithmVicinity5", GL_COMPUTE_SHADER);
			break;
		case Algorithm::Box5:
			shader_normals.setSubroutine("algorithmBox5", GL_COMPUTE_SHADER);
			break;
		case Algorithm::Radial:
			shader_normals.setSubroutine("algorithmRadial", GL_COMPUTE_SHADER);
			break;
		case Algorithm::PseudoRandomNormals:
			shader_normals.setSubroutine("algorithmPseudoRandomNormals", GL_COMPUTE_SHADER);
			break;
		default:
			break;
		}
		gpu_tex.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RG8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}
	GPUTexture2D* SparsePseudoRandomNormals::getOutput() {
		return &gpu_tex;
	}
	GLuint SparsePseudoRandomNormals::getOutputGL() {
		return gpu_tex.getGLobject();
	}
	void SparsePseudoRandomNormals::debugReloadShaders() {
		shader_normals.reload();
	}
	const std::string SparsePseudoRandomNormals::getAlgorithmName(Algorithm algo) {
		switch (algo) {
		case Algorithm::Vicinity3:
			return "Vicinity3";
			break;
		case Algorithm::Vicinity5:
			return "Vicinity5";
			break;
		case Algorithm::Box5:
			return "Box5";
			break;
		case Algorithm::Radial:
			return "Radial";
			break;
		case Algorithm::PseudoRandomNormals:
			return "PseudoRandomNormals";
			break;
		default:
			break;
		}
		return "";
	}

}
